# IOT Notification Auth

Frontend call `GET https://egc0fx8qdg.execute-api.ap-southeast-2.amazonaws.com/dev/patients/auth` endpoint to get short live credential to connect to IOT, the topicId is partnerId in the JWT token. Backend service sends notification to frontend through registered topicId

## API Documentation

[swagger](https://bitbucket.org/jz321158/emq-notification-auth/src/master/docs/swagger.yml?at=master&fileviewer=file-view-default)

## Error Codes Reference

[master source](https://bitbucket.org/jz321158/emq-notification-auth/src/master/src/exceptions/errorCodes.js?at=master&fileviewer=file-view-default)

## Get Started

```
npm i
npm run lint
npm t
```

## Build & Deploy

Bitbucket Pipeline definitions [./bitbucket-pipelines.yml](bitbucket-pipelines.yml)

**Deploy to all environments**

-   Triggered when pull request is merged to `master` branch
-   Manual trigger required when deploying to UAT and Prod

**Deploy feature branch to dev environment**

-   Go to Bitbucket Branches page
-   Click the "..." on the right side of the feature branch you want to deploy
-   Select "Run pipeline for branch"
-   Select custom: `deploy-to-dev`

## Run Test

`npm test` run unit, integration and component test  
`npm run test:unit` run unit test  
`npm run test:integration` run integration test  
`npm run test:component` run component test  
`npm run test:health` run healthcheck test
