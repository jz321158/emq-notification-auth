const R = require('ramda');
const {
  utils: { logHelper }
} = require('@jz/webapi-helper');
const { applyWebapiHandler } = require('./handlers/webapi');
const { applyErrorHandler } = require('./handlers/errorHandler');

/**
 * Entry handler for webapi and action handlers
 *
 * @returns response from webapi or action handler
 * @throws error if no handler found, or if action fails
 */
const handler = async event => {
  try {
    const foundHandler = R.cond([...applyWebapiHandler(event), [R.T, applyErrorHandler]]);
    return await foundHandler(event);
  } catch (error) {
    logHelper.log('index_handler', error, event);
    throw error;
  }
};

exports.handler = handler;
