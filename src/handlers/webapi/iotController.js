const { response, logHelper } = require('@jz/webapi-helper').utils;
const { isMatchEndpoint } = require('./utils');
const { iotAuthService } = require('../../domains/iotAuth');

/**
 * Gets handler mappings for the psa controller.
 * The event object is passed into the provided function when the endpoint is matched.
 *
 * @returns list of Ramda conditions to check if endpoint matches and map to the function
 */
const getHandlerMapping = () => [[isMatchEndpoint('GET', '/patients/auth'), getCredential]];

/**
 *
 * @param {object} event - lambda event
 * @param {string} event.httpMethod
 * @returns {object} success or failure response
 */
const getCredential = async event => {
  try {
    const { httpMethod } = event;

    const result = await iotAuthService.getCredential();
    return response.getSuccessfulLambdaProxyResponse({
      httpMethod,
      result
    });
  } catch (error) {
    logHelper.log('iotController_getCredential', error, event);
    return response.getFailureLambdaProxyResponse({ error });
  }
};

module.exports = {
  getHandlerMapping,
  getCredential
};
