const healthCheckController = require('./healthCheckController');
const iotController = require('./iotController');

/**
 * Returns an array of handler mappings for all webapi controllers
 *
 * @param {object} event
 */
const applyWebapiHandler = event => [
  ...healthCheckController.getHandlerMapping(event),
  ...iotController.getHandlerMapping(event)
];

module.exports = {
  applyWebapiHandler
};
