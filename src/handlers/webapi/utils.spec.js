const { expect } = require('chai');
const utils = require('./utils');

describe('handlers/webapi/utils', () => {
  describe('isMatchEndpoint', () => {
    const resource = 'some/resource';
    const httpMethod = 'GET';
    it('will successfully match with matching event', () => {
      const event = {
        resource,
        httpMethod
      };

      const result = utils.isMatchEndpoint(httpMethod, resource)(event);

      expect(result).to.equal(true);
    });
    it('will fail to match with non-matching event', () => {
      const event = {
        resource: 'some/other/resource',
        httpMethod: 'GET'
      };

      const result = utils.isMatchEndpoint(httpMethod, resource)(event);

      expect(result).to.equal(false);
    });
  });
});
