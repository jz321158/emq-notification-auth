const { response, logHelper } = require('@jz/webapi-helper').utils;
const { isMatchEndpoint } = require('./utils');
const { healthCheckService } = require('../../domains/healthCheck');

/**
 * Gets handler mappings for the healthcheck controller.
 * The event object is passed into the provided function when the endpoint is matched.
 *
 * @returns list of Ramda conditions to check if endpoint matches and map to the function
 */
const getHandlerMapping = () => [[isMatchEndpoint('GET', '/health'), healthCheck]];

/**
 *
 * @param {object} event - lambda event
 * @param {string} event.httpMethod
 * @returns {object} success or failure response
 */
const healthCheck = async event => {
  try {
    const { httpMethod } = event;
    const result = await healthCheckService.getCheckResult();
    return response.getSuccessfulLambdaProxyResponse({ httpMethod, result });
  } catch (error) {
    logHelper.log('healthCheckController_healthCheck', error, event);
    return response.getFailureLambdaProxyResponse({ error });
  }
};

module.exports = {
  healthCheck,
  getHandlerMapping
};
