const sinon = require('sinon');
const { expect } = require('chai');
const healthCheckController = require('./healthCheckController');
const { healthCheckService } = require('../../domains/healthCheck');

const sandbox = sinon.createSandbox();

describe('healthCheckController', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('healthCheck', () => {
    it('will return a successful response on success', async () => {
      const event = {
        httpMethod: 'GET'
      };

      sandbox
        .stub(healthCheckService, 'getCheckResult')
        .resolves({ service: 'IOT Notification Auth Service', status: 'UP' });

      const response = await healthCheckController.healthCheck(event);

      expect(response).to.deep.equal({
        body: '{"service":"IOT Notification Auth Service","status":"UP"}',
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Cache-Control': 'private, no-store'
        },
        statusCode: 200,
        isBase64Encoded: false
      });
    });

    it('will return a failure response on failure', async () => {
      const event = {
        httpMethod: 'GET'
      };

      sandbox.stub(healthCheckService, 'getCheckResult').rejects(new Error());

      const response = await healthCheckController.healthCheck(event);

      expect(response).to.deep.equal({
        body: '{"message":"","statusCode":500}',
        headers: { 'Access-Control-Allow-Origin': '*', 'Cache-Control': 'private, no-store' },
        statusCode: 500
      });
    });
  });
});
