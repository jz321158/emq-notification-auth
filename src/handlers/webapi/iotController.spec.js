const { expect } = require('chai');
const sinon = require('sinon');
const { iotAuthService } = require('../../domains/iotAuth');
const controller = require('./iotController');

const sandbox = sinon.createSandbox();

describe('iotController', () => {
  describe('getCredential', () => {
    let event;

    beforeEach(() => {
      event = {
        httpMethod: 'GET'
      };
    });

    afterEach(() => {
      sandbox.restore();
    });

    it('should return status code 200 when there is no error', async () => {
      const expectedResult = {
        accessKeyId: 'accessKeyId',
        host: 'host',
        region: 'region',
        secretKey: 'secretKey',
        sessionToken: 'sessionToken',
        topic: 'topic'
      };

      const iotAuthServiceMock = sandbox.mock(iotAuthService);

      iotAuthServiceMock
        .expects('getCredential')
        .once()
        .returns(expectedResult);

      const response = await controller.getCredential(event);

      expect(iotAuthServiceMock.verify()).to.equal(true);
      expect(response).to.deep.equal({
        body: JSON.stringify(expectedResult),
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Cache-Control': 'private, no-store'
        },
        statusCode: 200,
        isBase64Encoded: false
      });
    });
  });
});
