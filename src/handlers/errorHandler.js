const { exceptions } = require('@jz/webapi-helper');
const errorCodes = require('../exceptions/errorCodes');

const applyErrorHandler = () => {
  throw new exceptions.InternalServerError(errorCodes.noHandlerMapping.message, errorCodes.noHandlerMapping.code);
};

module.exports = {
  applyErrorHandler
};
