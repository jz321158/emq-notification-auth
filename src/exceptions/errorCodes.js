/**
 * Placeholder for customized error codes
 * Each microservice reserve 1000 error codes
 *
 * Error code range start from 118001
 *
 */
module.exports = {
  noHandlerMapping: {
    code: 118001,
    message: 'No found handler mapping for action or resource/method'
  }
};
