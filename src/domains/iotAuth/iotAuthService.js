const iot = require('../../gateways/iot');
const sts = require('../../gateways/sts');

/**
 * get IOT temp credential, its valid for 30 mins
 *
 */
const getCredential = async () => {
  const host = await iot.getEndpointAddress();
  const topicName = getTopicName();
  const role = await sts.assumeRole(topicName);
  const result = formatResult({ host, role, topicName });

  return result;
};

const getTopicName = () => 'patient';

const formatResult = ({ host, role, topicName }) => ({
  host,
  region: process.env.region || 'ap-southeast-2',
  accessKeyId: role.accessKeyId,
  secretKey: role.secretKey,
  sessionToken: role.sessionToken,
  topic: topicName
});

module.exports = {
  getCredential
};
