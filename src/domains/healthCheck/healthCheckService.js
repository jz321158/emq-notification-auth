const healthCheckHelper = require('@jz/healthcheck-helper');

const getIndicators = () => [];

const getCheckResult = () => healthCheckHelper.check('IOT Notification Auth Service', getIndicators());

module.exports = {
  getCheckResult
};
