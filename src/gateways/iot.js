const AWS = require('aws-sdk');

AWS.config.region = process.env.region || 'ap-southeast-2';

const iot = new AWS.Iot();

const getEndpointAddress = async () => {
  const iotEndpoint = await iot.describeEndpoint({ endpointType: 'iot:Data-ATS' }).promise();

  return iotEndpoint.endpointAddress;
};

module.exports = {
  getEndpointAddress
};
