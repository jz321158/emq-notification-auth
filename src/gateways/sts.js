const AWS = require('aws-sdk');

const roleName = 'frontend-iot';
const region = process.env.region || 'ap-southeast-2';
AWS.config.region = region;

const sts = new AWS.STS();

const getRandomInt = () => Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);

const assumeRole = async topicName => {
  const identity = await sts.getCallerIdentity({}).promise();

  const params = {
    RoleArn: `arn:aws:iam::${identity.Account}:role/${roleName}`,
    Policy: JSON.stringify({
      Version: '2012-10-17',
      Statement: [
        {
          Action: ['iot:Connect'],
          Resource: '*',
          Effect: 'Allow'
        },
        {
          Action: ['iot:Connect', 'iot:Subscribe', 'iot:Publish', 'iot:Receive'],
          Resource: [`arn:aws:iot:${region}:${identity.Account}:*/${topicName}`],
          Effect: 'Allow'
        }
      ]
    }),
    RoleSessionName: getRandomInt().toString()
  };

  const stsRole = await sts.assumeRole(params).promise();

  return {
    accessKeyId: stsRole.Credentials.AccessKeyId,
    secretKey: stsRole.Credentials.SecretAccessKey,
    sessionToken: stsRole.Credentials.SessionToken
  };
};

module.exports = {
  assumeRole
};
