const { handler } = require('../../src/index');

/**
 * Execute the handler with the provided paramaters
 *
 * @param {Object} params
 * @param {string} params.httpMethod
 * @param {string} params.resourcePath
 * @param {Object} params.body
 * @param {Object} params.pathParameters
 * @param {Object} params.queryStringParameters
 * @param {Object} params.stageVariables
 * @param {Object} params.headers
 * @returns {Promise}
 */
const executeApiEvent = params =>
  handler({
    resource: params.resourcePath,
    path: params.resourcePath,
    body: params.body,
    httpMethod: params.httpMethod,
    pathParameters: params.pathParameters,
    requestContext: {
      path: params.resourcePath,
      httpMethod: params.httpMethod,
      resourcePath: params.resourcePath,
      authorizer: params.authorizer
    },
    queryStringParameters: params.queryStringParameters,
    headers: params.headers
  });

module.exports = {
  executeApiEvent
};
