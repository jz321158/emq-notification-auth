const { expect } = require('chai');
const testHelper = require('./testHelper');

describe('GET /health', () => {
  it('200 success', async () => {
    const resourcePath = '/health';
    const httpMethod = 'GET';

    const response = await testHelper.executeApiEvent({
      httpMethod,
      resourcePath
    });

    expect(response).to.have.keys(['body', 'headers', 'statusCode', 'isBase64Encoded']);
    expect(JSON.parse(response.body).status).to.equal('UP');
    expect(response.statusCode).to.equal(200);
    expect(response.headers).to.deep.equal({
      'Access-Control-Allow-Origin': '*',
      'Cache-Control': 'private, no-store'
    });
  });
});
