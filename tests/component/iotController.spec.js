const sinon = require('sinon');
const { expect } = require('chai');
const testHelper = require('./testHelper');
const iotAuthService = require('../../src/domains/iotAuth/iotAuthService');

const sandbox = sinon.createSandbox();

describe('GET /patients/auth', () => {
  let event;

  beforeEach(() => {
    event = {
      httpMethod: 'GET',
      resourcePath: '/patients/auth'
    };
  });

  it('200 success', async () => {
    const response = await testHelper.executeApiEvent(event);

    const body = JSON.parse(response.body);
    expect(response.statusCode).to.equal(200);
    expect(body).to.have.all.keys('accessKeyId', 'host', 'region', 'secretKey', 'sessionToken', 'topic');
    expect(body.region).to.equal('ap-southeast-2');
    expect(body.host).to.equal('a381cmy4oifop6-ats.iot.ap-southeast-2.amazonaws.com');
    expect(body.topic).to.equal('patient'); // decode from auth token
    expect(body.region).to.equal('ap-southeast-2');

    expect(response.headers).to.deep.equal({
      'Access-Control-Allow-Origin': '*',
      'Cache-Control': 'private, no-store'
    });

    expect(response.isBase64Encoded).to.equal(false);
  });

  it('500 error', async () => {
    sandbox.stub(iotAuthService, 'getCredential').rejects(new Error('app error'));

    const response = await testHelper.executeApiEvent(event);

    const body = JSON.parse(response.body);
    expect(response.statusCode).to.equal(500);
    expect(response).to.have.keys(['body', 'headers', 'statusCode']);
    expect(body).to.deep.equal({
      message: 'app error',
      statusCode: 500
    });

    expect(response.headers).to.deep.equal({
      'Access-Control-Allow-Origin': '*',
      'Cache-Control': 'private, no-store'
    });
  });
});
