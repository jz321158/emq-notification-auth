const { expect } = require('chai');
const rp = require('request-promise-native');

const { stage } = process.env;

describe('HealthCheck Test', () => {
  it('all health checks are successful and status is UP', async () => {
    const options = {
      uri: `https://egc0fx8qdg.execute-api.ap-southeast-2.amazonaws.com/${stage}/health`,
      resolveWithFullResponse: true,
      json: true
    };

    const checkResponse = await rp(options);
    expect(checkResponse.statusCode).to.deep.equal(200);
    expect(checkResponse.body.status).to.deep.equal('UP');
  });
});
